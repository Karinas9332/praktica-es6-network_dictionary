const englishInput = document.getElementById('input-eng'),
    russianInput = document.getElementById('input-rus'),
    inputs = document.querySelectorAll('input'),
    saveButton = document.getElementById('btn'),
    table = document.getElementById('table');
let delButtons;
let words = [];
localStorage.length < 1 ? words = [] : words = JSON.parse(localStorage.getItem('words'));
// console.log(words);

const addWordToTable = i => {
    table.innerHTML += `
    <tr>
    <td>${words[i].translate}</td>
    <td class="text">${words[i].russian}
    <button class="button-del">x</button>
    </td>
    </tr>
    `
}
words.forEach((item, index) => {
    addWordToTable(index);
});

class createWord {
    constructor(translate, russian) {
        this.translate = translate;
        this.russian = russian;
    }
}

saveButton.addEventListener('click', () => {
    if (
        englishInput.value.length < 1 ||
        russianInput.value.length < 1 ||
        !isNaN(englishInput.value) ||
        !isNaN(russianInput.value)) {
        for (let key of inputs) {
            key.classList.add('error');
        }
    }
    else {
        for (let key of inputs) {
            key.classList.remove('error');
        }
        words.push(new createWord(englishInput.value, russianInput.value));
        localStorage.setItem('words', JSON.stringify(words));
        addWordToTable(words.length - 1);
        deleteWord()
    }
    inputs[0].value = '';
    inputs[1].value = '';
})

deleteWord();
function deleteWord() {
    delButtons = document.querySelectorAll('.button-del');
    for (let j = 0; j < delButtons.length; j++) {
        delButtons[j].addEventListener('click', () => {
            words.splice(j, 1);
            localStorage.setItem('words', JSON.stringify(words));
            table.innerHTML = ''
            for (let l = 0; l < words.length; l++) {
                addWordToTable(l);
            }
            deleteWord()
        })
    }
}


